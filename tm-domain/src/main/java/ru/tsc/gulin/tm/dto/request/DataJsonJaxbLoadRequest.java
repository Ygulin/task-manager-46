package ru.tsc.gulin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataJsonJaxbLoadRequest extends AbstractUserRequest {

    public DataJsonJaxbLoadRequest(@Nullable final String token) {
        super(token);
    }

}
