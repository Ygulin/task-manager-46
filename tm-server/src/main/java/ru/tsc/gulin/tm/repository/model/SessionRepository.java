package ru.tsc.gulin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.model.ISessionRepository;
import ru.tsc.gulin.tm.enumerated.Sort;
import ru.tsc.gulin.tm.model.Project;
import ru.tsc.gulin.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        return entityManager.createQuery("SELECT m  FROM Session m", Session.class).getResultList();
    }

    @NotNull
    @Override
    public List<Session> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) return Collections.emptyList();
        return entityManager.createQuery("SELECT m FROM Session m WHERE m.user.id = :userId", Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Session> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull String jpql = "SELECT m FROM Session m WHERE m.user.id = :userId ORDER BY m."
                + getSortType(sort.getComparator());
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull final String id) {
        return entityManager.find(Session.class, id);
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty() || id.isEmpty()) return null;
        return entityManager
                .createQuery("SELECT m FROM Session m WHERE m.user.id = :userId AND m.id = :id", Session.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.remove(entityManager.getReference(Session.class, id));
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        entityManager.remove(findOneById(userId, id));
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM Session", Session.class).executeUpdate();
    }

    @Override
    public void clear(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM Session m WHERE m.user.id = :userId", Session.class)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return entityManager.createQuery("SELECT COUNT(m) = 1 FROM Session m WHERE m.id = :id", Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return entityManager
                .createQuery("SELECT COUNT(m) = 1 FROM Session m WHERE m.id = :id AND m.user.id = :userId", Boolean.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(m) FROM Session", Long.class)
                .getSingleResult();
    }

    @Override
    public long getSize(@NotNull String userId) {
        return entityManager.createQuery("SELECT COUNT(m) FROM Session m WHERE m.user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

}
