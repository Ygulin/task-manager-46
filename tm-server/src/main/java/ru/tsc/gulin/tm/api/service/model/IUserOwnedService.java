package ru.tsc.gulin.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.model.IUserOwnedRepository;
import ru.tsc.gulin.tm.model.AbstractUserOwnedModel;

import java.util.Date;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

}
