package ru.tsc.gulin.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.dto.ISessionRepositoryDTO;
import ru.tsc.gulin.tm.api.service.IConnectionService;
import ru.tsc.gulin.tm.api.service.dto.ISessionServiceDTO;
import ru.tsc.gulin.tm.dto.model.ProjectDTO;
import ru.tsc.gulin.tm.dto.model.SessionDTO;
import ru.tsc.gulin.tm.exception.field.NameEmptyException;
import ru.tsc.gulin.tm.exception.field.UserIdEmptyException;
import ru.tsc.gulin.tm.repository.dto.ProjectRepositoryDTO;
import ru.tsc.gulin.tm.repository.dto.SessionRepositoryDTO;

import javax.persistence.EntityManager;
import java.util.Optional;

public class SessionServiceDTO extends AbstractUserOwnedServiceDTO<SessionDTO, ISessionRepositoryDTO> implements ISessionServiceDTO {

    public SessionServiceDTO(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected SessionRepositoryDTO getRepository(@NotNull final EntityManager entityManager) {
        return new SessionRepositoryDTO(entityManager);
    }

}
