package ru.tsc.gulin.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.model.ITaskRepository;
import ru.tsc.gulin.tm.api.repository.model.IUserRepository;
import ru.tsc.gulin.tm.api.service.IConnectionService;
import ru.tsc.gulin.tm.api.service.model.ITaskService;
import ru.tsc.gulin.tm.enumerated.Status;
import ru.tsc.gulin.tm.exception.field.*;
import ru.tsc.gulin.tm.model.Task;
import ru.tsc.gulin.tm.repository.model.TaskRepository;
import ru.tsc.gulin.tm.repository.model.UserRepository;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected TaskRepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskRepository(entityManager);
    }

    @NotNull
    protected IUserRepository getUserRepository(@NotNull final EntityManager entityManager) {
        return new UserRepository(entityManager);
    }

    @NotNull
    @Override
    public Task create(@Nullable final String userId, @Nullable final String name) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).filter(item -> !item.isEmpty()).orElseThrow(NameEmptyException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final Task task = new Task();
        task.setUser(getUserRepository(getEntityManager()).findOneById(userId));
        task.setName(name);
        try {
            @NotNull final TaskRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).filter(item -> !item.isEmpty()).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).filter(item -> !item.isEmpty()).orElseThrow(DescriptionEmptyException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final Task task = new Task();
        task.setUser(getUserRepository(getEntityManager()).findOneById(userId));
        task.setName(name);
        task.setDescription(description);
        try {
            @NotNull final TaskRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateStart,
            @Nullable final Date dateEnd
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).filter(item -> !item.isEmpty()).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).filter(item -> !item.isEmpty()).orElseThrow(DescriptionEmptyException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final Task task = new Task();
        task.setUser(getUserRepository(getEntityManager()).findOneById(userId));
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(dateStart);
        task.setDateEnd(dateEnd);
        try {
            @NotNull final TaskRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).filter(item -> !item.isEmpty()).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).filter(item -> !item.isEmpty()).orElseThrow(DescriptionEmptyException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final Task task = findOneById(userId, id);
        task.setName(name);
        task.setDescription(description);
        try {
            @NotNull final TaskRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(status).orElseThrow(StatusEmptyException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final Task task = findOneById(userId, id);
        task.setStatus(status);
        try {
            @NotNull final TaskRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final TaskRepository repository = getRepository(entityManager);
            return repository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

}
